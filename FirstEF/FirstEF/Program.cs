﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            // instance of dbcontext
            PeopleDatabaseContext ctx = new PeopleDatabaseContext();

            Person person = new Person() { Age = new Random().Next(5, 60), Name = "John" };
            ctx.people.Add(person);

            Home address = new Home() { Address = "Main street" };
            ctx.homes.Add(address);

            ctx.SaveChanges();
            Console.WriteLine("record is added");


            // fetch data, need write LINQ
            Person FetchedP = (from p in ctx.people where p.Id == 1 select p).FirstOrDefault<Person>();
            if (FetchedP != null)
            {
                FetchedP.Name += "Boulay";
                ctx.SaveChanges();

                Console.WriteLine("record updated");
            }
            else
            {
                Console.WriteLine("record not found");
            }

            // delete
            Person pToDelete1 = (from p in ctx.people where p.Id == 2 select p).FirstOrDefault<Person>();
            Person pToDelete2 = ctx.people.Where(p => p.Id == 2).FirstOrDefault<Person>();

            if (pToDelete1 != null)
            {
                ctx.people.Remove(pToDelete1);
                ctx.SaveChanges();
                Console.WriteLine("record deleted");
            }
            else
            {
                Console.WriteLine("record not found");
            }


            // fetch all person
            List<Person> people1 = (from p in ctx.people select p).ToList<Person>();
            List<Person> people2 = ctx.people.ToList<Person>();

            people1.ForEach(p => Console.WriteLine($"{p.Id} and {p.Name} and {p.Age}"));


        }
    }
}
