﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstEF
{
    class PeopleDatabaseContext : DbContext     //using System.Data.Entity;
    {
        // set db to project folder
        const string DbName = "testDB.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);

        static string connectionStr = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30";
        public PeopleDatabaseContext() : base(connectionStr)
        {
        }

        // default location: user foler
        /*public PeopleDatabaseContext() : base("PeopleDatabaseContext1")
        {
        }
*/
        public DbSet<Person> people { get; set; }
        public DbSet<Home> homes { get; set; }

    }
}
