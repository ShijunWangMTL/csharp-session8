﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstEF
{
    class Person  //table name
    {
        // add columns
        // KEY
        public int Id { get; set; }

        [Required]  //using System.ComponentModel.DataAnnotations;
        [StringLength(50)]
        public string Name { get; set; }
        public int Age { get; set; }



    }

    public class Home
    {
        public int Id { get; set; }
        public string Address { get; set; }
    }
}
