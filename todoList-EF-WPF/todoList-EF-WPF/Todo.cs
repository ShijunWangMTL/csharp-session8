﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todoList_EF_WPF
{
    class Todo
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Task { get; set; }
        public int Difficulty { get; set; }
        public string Due { get; set; }
        public string Status { get; set; }



    }
}
