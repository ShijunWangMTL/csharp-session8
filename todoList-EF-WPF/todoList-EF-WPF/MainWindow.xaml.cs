﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace todoList_EF_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Todo> todoList = new List<Todo>();
        TodolistDbContext ctx = new TodolistDbContext();
        public MainWindow()
        {
            InitializeComponent();
            // read data from DB           
            try
            {
                todoList = (from td in ctx.todolist select td).ToList<Todo>();
                lvGridTodo.ItemsSource = todoList;
            }
            catch (Exception exc)
            {
                return;
            }


        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            Todo tdToAdd = new Todo();

            if (txtTask.Text.Equals(""))
            {
                MessageBox.Show("Please enter the task.");
                return;
            }

            tdToAdd.Task = txtTask.Text;

            tdToAdd.Difficulty = (int)sliderDifficulty.Value;

            DateTime? selectedDate = dueDatePicker.SelectedDate;
            if (selectedDate == null)
            {
                MessageBox.Show("Please choose a date.");
                return;
            }

            tdToAdd.Due = selectedDate.Value.ToString("yyyy-MM-dd");

            tdToAdd.Status = cmbStatus.Text;

            ctx.todolist.Add(tdToAdd);
            ctx.SaveChanges();

            todoList.Add(tdToAdd);
            ResetView();

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvGridTodo.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a task.");
                return;
            }

            Todo tdToDelete = (Todo)lvGridTodo.SelectedItem;

            if (tdToDelete != null)
            {

                MessageBoxResult result = MessageBox.Show("Are you sure to delete this task?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Error);
                if (result == MessageBoxResult.Yes)
                {
                    ctx.todolist.Remove(tdToDelete);
                    ctx.SaveChanges();

                    todoList.Remove(tdToDelete);
                }
            }
            ResetView();
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvGridTodo.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a task.");
                return;
            }

            Todo tdToUpdate = (Todo)lvGridTodo.SelectedItem;

            if (tdToUpdate != null)
            {
                if (txtTask.Text.Equals(""))
                {
                    MessageBox.Show("Please enter the task.");
                    return;
                }

                tdToUpdate.Task = txtTask.Text;

                tdToUpdate.Difficulty = (int)sliderDifficulty.Value;

                DateTime? selectedDate = dueDatePicker.SelectedDate;
                if (selectedDate == null)
                {
                    MessageBox.Show("Please choose a date.");
                    return;
                }

                tdToUpdate.Due = selectedDate.Value.ToString("yyyy-MM-dd");

                tdToUpdate.Status = cmbStatus.Text;

                ctx.todolist.Add(tdToUpdate);
                ctx.Entry(tdToUpdate).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();

                ResetView();
            }
        }

        private void LvGridTodo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnDelete.IsEnabled = true;
            btnUpdate.IsEnabled = true;

            var selectedTodo = lvGridTodo.SelectedItem;
            if (selectedTodo is Todo)
            {
                Todo td = (Todo)selectedTodo;
                txtTask.Text = td.Task;
                sliderDifficulty.Value = td.Difficulty;
                dueDatePicker.SelectedDate = DateTime.Parse(td.Due);
                cmbStatus.Text = td.Status;
            }
        }

        private void ResetView()
        {
            lvGridTodo.Items.Refresh();

            txtTask.Clear();
            sliderDifficulty.Value = 1;
            dueDatePicker.SelectedDate = null;
            cmbStatus.Text = "Not started";

            lvGridTodo.SelectedIndex = -1;

            btnDelete.IsEnabled = false;
            btnUpdate.IsEnabled = false;

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }



    }
}
