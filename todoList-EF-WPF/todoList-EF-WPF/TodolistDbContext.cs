﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace todoList_EF_WPF
{
    class TodolistDbContext : DbContext
    {
        // set DB into the project folder
        const string DbName = "todolistDb.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        static string connectionStr = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30";

        public TodolistDbContext() : base(connectionStr)
        {

        }

        public DbSet<Todo> todolist { get; set; }

    }
}
